"""Do Something."""

from tempfile import TemporaryDirectory

import pydicom


class Doodalyzer:
    """Strange class."""
    def doodle(self, value: int):
        """This function is covered by tests."""
        with TemporaryDirectory() as dir:
            retval = value + 3
        return retval
    
    def untested(self, unused: str):
        """This function is not covered by tests."""

        return "Kratzy"
