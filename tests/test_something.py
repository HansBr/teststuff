"""Test file."""
import unittest

import xmlrunner
from src.do_something import Doodalyzer


class TestSomethint(unittest.TestCase):
    """Test class."""
    def test_something(self):
        """Test function."""
        dood = Doodalyzer()
        self.assertEqual(dood.doodle(5), 8)


if __name__ == "__main__":
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
